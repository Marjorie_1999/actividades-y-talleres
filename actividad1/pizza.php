<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>HTML Forms</title>
</head>
<body>
    <div class="container mx-auto p-12 max-w-md">
        <h1 class="text-4xl mb-8 text-center">Thank You</h1>
        <hr class="mb-8">
        <p>Thank you for ordering from Black goose bistro. We have received the following information about your order:</p>
        <h2 class="text-2xl mt-8 text-red-700">Your Information:</h2>
        <ul>
            <li>Name: <?=$_POST["customer_name"]?></li>
            <li>Address: <?=$_POST["address"]?></li>
            <li>Telephone: <?=$_POST["telephone"]?></li>
            <li>Email: <?=$_POST["email"]?></li>
        </ul>
        <p>Delivery instructions: <?=$_POST["instructions"]?></p>
        <h2 class="text-2xl mt-8 text-red-700">Your Pizza:</h2>
        <ul>
            <li>Crust: <?=$_POST["crust"]?></li>
            <li>Toppings: <?php
            if(!empty($_POST["toppings"])) {
                echo implode(", ", $_POST["toppings"]);
            }
            else {
                echo "None";
            }
            ?></li>
            <li>Number of pizzas: <?=$_POST["quantity"]?></li>
        </ul>
    </div>
</body>
</html>
